/*******************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Ernesto Cruz Olivera (ecruzolivera@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

/**
 ******************************************************************************
 * @file    Test_Delegate_Void.cpp
 * @author  Ing. Manuel Alejandro Linares Paez
 * @version 1.0
 * @date    Mar 29, 2016
 * @brief   Test for Delegate Class.
 ******************************************************************************
 */
#include <gtest/gtest.h>

#include "Delegate.hpp"

bool testGlobalVoid         = false;
bool testStaticVoid         = false;
bool testPublicVoid         = false;
bool testVirtualBaseVoid    = false;
bool testVirtualDerivedvoid = false;
bool testConstVoid          = false;

void GlobalFunction(void)
{
  testGlobalVoid = true;
}

class Base0
{
 public:
  static void staticMember(void)
  {
    testStaticVoid = true;
  }

  void publicMember(void)
  {
    testPublicVoid = true;
  }
};

class Base1
{
 public:
  virtual void virtualMemberVoid(void)
  {
    testVirtualBaseVoid = true;
  }
};

class Derived : public Base0, Base1
{
 public:
  virtual void virtualMemberVoid(void)
  {
    testVirtualDerivedvoid = true;
  }
  void constMember(void) const
  {
    testConstVoid = true;
  }
};

using namespace eHSM;

class DelegateVoid : public ::testing::Test
{
 public:
  Base0   objBase0void;
  Base1   objBase1void;
  Derived objDerivedvoid;
};

TEST_F(DelegateVoid, TestFromGlobalFunction)
{
  Delegate<void> exDelegate;
  exDelegate();
  EXPECT_FALSE(testGlobalVoid);
  exDelegate.bind<&GlobalFunction>();
  exDelegate();
  EXPECT_TRUE(testGlobalVoid);
}

TEST_F(DelegateVoid, TestFromStaticFunction)
{
  Delegate<void> exDelegate;
  exDelegate();
  EXPECT_FALSE(testStaticVoid);
  exDelegate.bind<&Base0::staticMember>();
  exDelegate();
  EXPECT_TRUE(testStaticVoid);
}

TEST_F(DelegateVoid, TestFromMember)
{
  Delegate<void> exDelegate;
  exDelegate();
  EXPECT_FALSE(testPublicVoid);
  exDelegate.bind<Base0, &Base0::publicMember>(&objBase0void);
  exDelegate();
  EXPECT_TRUE(testPublicVoid);
}

TEST_F(DelegateVoid, TestFromVirtualMember)
{
  Delegate<void> exDelegate;
  exDelegate();
  EXPECT_FALSE(testVirtualBaseVoid);
  exDelegate.bind<Base1, &Base1::virtualMemberVoid>(&objBase1void);
  exDelegate();
  EXPECT_TRUE(testVirtualBaseVoid);
}

TEST_F(DelegateVoid, TestFromConstMember)
{
  Delegate<void> exDelegate;
  exDelegate();
  EXPECT_FALSE(testConstVoid);
  exDelegate.bind<Derived, &Derived::constMember>(&objDerivedvoid);
  exDelegate();
  EXPECT_TRUE(testConstVoid);
}

TEST_F(DelegateVoid, TestFromVirtualMemberReimplemented)
{
  Delegate<void> exDelegate;
  exDelegate();
  EXPECT_FALSE(testVirtualDerivedvoid);
  exDelegate.bind<Derived, &Derived::virtualMemberVoid>(&objDerivedvoid);
  exDelegate();
  EXPECT_TRUE(testVirtualDerivedvoid);
}

TEST_F(DelegateVoid, OperatorIqual)
{
  Delegate<void> exDelegate;
  exDelegate();
  exDelegate.bind<Derived, &Derived::virtualMemberVoid>(&objDerivedvoid);
  Delegate<void> tempDelegate;
  tempDelegate.bind<Derived, &Derived::virtualMemberVoid>(&objDerivedvoid);
  EXPECT_TRUE(exDelegate == tempDelegate);
  tempDelegate.bind<Derived, &Derived::constMember>(&objDerivedvoid);
  EXPECT_FALSE(exDelegate == tempDelegate);
}
