/*******************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Ernesto Cruz Olivera (ecruzolivera@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#include <gtest/gtest.h>
#include <stdint.h>

#include <cstring>

#include "RingBuffer.hpp"
using namespace eHSM;
class RingbufferInt16Type : public ::testing::Test
{
 public:
  typedef int16_t                                BufferType;
  static const size_t                            ringBufferLEN = 13;
  Declare::RingBuffer<BufferType, ringBufferLEN> ringBuffer;
};

TEST_F(RingbufferInt16Type, EmptyAfterCreation)
{
  EXPECT_TRUE(ringBuffer.isEmpty());
}

TEST_F(RingbufferInt16Type, NotFullAfterCreation)
{
  EXPECT_FALSE(ringBuffer.isFull());
}

TEST_F(RingbufferInt16Type, InsertOneItemAndRetrieveIt)
{
  BufferType item          = 10000;
  uint32_t   items_written = 1;
  BufferType ret_item      = 0;
  EXPECT_EQ(items_written, ringBuffer.write(item));
  EXPECT_EQ(items_written, ringBuffer.read(ret_item));
  EXPECT_EQ(item, ret_item);
}

TEST_F(RingbufferInt16Type, InsertOneItemAndCheckNotEmptyAndNotFull)
{
  BufferType item = 55;
  EXPECT_EQ(1u, ringBuffer.write(item));
  EXPECT_FALSE(ringBuffer.isEmpty());
  EXPECT_FALSE(ringBuffer.isFull());
}

TEST_F(RingbufferInt16Type, InsertSeveralItemsAndRetrieveThem)
{
  BufferType     items[]              = {55, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  const uint32_t ITEMS_LEN            = 10;
  BufferType     ret_items[ITEMS_LEN] = {0};
  EXPECT_EQ(ITEMS_LEN, ringBuffer.write(items, ITEMS_LEN));
  EXPECT_EQ(ITEMS_LEN, ringBuffer.read(ret_items, ITEMS_LEN));
  EXPECT_EQ(0, memcmp(items, ret_items, ITEMS_LEN));
}

TEST_F(RingbufferInt16Type, InsertSeveralItemsAndRetrieveThemAndInsertMoreUntilOverlapTheBuffer)
{
  BufferType     items[]              = {55, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  const uint32_t ITEMS_LEN            = 10;
  BufferType     ret_items[ITEMS_LEN] = {0};
  ringBuffer.write(items, ITEMS_LEN);
  ringBuffer.read(ret_items, ITEMS_LEN);
  /* do it again for overlaping*/
  EXPECT_EQ(ITEMS_LEN, ringBuffer.write(items, ITEMS_LEN));
  EXPECT_EQ(ITEMS_LEN, ringBuffer.read(ret_items, ITEMS_LEN));
  EXPECT_EQ(0, memcmp(items, ret_items, ITEMS_LEN));
}

TEST_F(RingbufferInt16Type, InsertSeveralItemsOverlapCheckBufferSizeSpaceLeftAndItemsAviable)
{
  BufferType     items[]              = {55, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  const uint32_t ITEMS_LEN            = 10;
  BufferType     ret_items[ITEMS_LEN] = {0};
  ringBuffer.write(items, ITEMS_LEN);
  ringBuffer.read(ret_items, ITEMS_LEN);
  ringBuffer.write(items, ITEMS_LEN);
  EXPECT_EQ(ITEMS_LEN, ringBuffer.itemsAviable());
  EXPECT_EQ(ringBufferLEN - ITEMS_LEN, ringBuffer.spaceLeft());
}
