project(Tests)
# Unit testing should be made in debug mode
set(CMAKE_BUILD_TYPE "Debug")

include(FetchContent)
FetchContent_Declare(
  googletest
  GIT_REPOSITORY https://github.com/google/googletest.git
  GIT_TAG release-1.8.0)

set(gtest_force_shared_crt
    ON
    CACHE BOOL "" FORCE)
set(BUILD_GMOCK
    OFF
    CACHE BOOL "" FORCE)
set(BUILD_GTEST
    ON
    CACHE BOOL "" FORCE)

FetchContent_MakeAvailable(googletest)

# GTEST
file(GLOB TEST_SRC_FILES ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp)


add_executable(${PROJECT_NAME} ${TEST_SRC_FILES})
target_link_libraries(${PROJECT_NAME} ${CMAKE_PROJECT_NAME} gtest)
add_test(NAME ${PROJECT_NAME}_test COMMAND ${PROJECT_NAME})
