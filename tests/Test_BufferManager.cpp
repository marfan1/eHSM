/*******************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Ernesto Cruz Olivera (ecruzolivera@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

/**
 ******************************************************************************
 * @file    Test_BufferManager.cpp
 * @author  Ernesto Cruz Olivera
 * @version 1.0
 * @date    Jan 6, 2016
 * @brief   Tests for BufferManager Class
 ******************************************************************************
 */
#include <cstring>

#include "BufferManager.hpp"
#include "gtest/gtest.h"
using namespace eHSM;
using namespace std;
class TestBufferManager : public ::testing::Test
{
 public:
  typedef uint16_t    ItemType;
  static const size_t BUFFER_SIZE = 10;
  static const size_t BUFFER_QTTY = 3;

  BufferManager<ItemType, BUFFER_SIZE, BUFFER_QTTY, ::Declare::DmaBuffer> buffer;
};

TEST_F(TestBufferManager, EmptyAfterCreation)
{
  EXPECT_TRUE(buffer.buffersFullAviable() == 0);
  EXPECT_TRUE(buffer.buffersEmptyAviable() == BUFFER_QTTY);
}

TEST_F(TestBufferManager, NotFullAfterCreation)
{
  EXPECT_FALSE(buffer.buffersEmptyAviable() == 0);
}

TEST_F(TestBufferManager, InsertOneItemAndRetrieveIt)
{
  ItemType item          = 10000;
  uint32_t items_written = buffer.write(item);
  buffer.nextBufferForWrite();
  DmaBuffer<ItemType> *lBuffer = buffer.buffer();
  assert(lBuffer);
  ItemType *data = lBuffer->data();
  EXPECT_EQ(items_written, 1);
  EXPECT_EQ(item, *data);
}

TEST_F(TestBufferManager, FillOneBufferAndCheckNumberOfBuffersAviable)
{
  ItemType items[BUFFER_SIZE] = {55, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  buffer.write(items, BUFFER_SIZE);

  EXPECT_EQ((BUFFER_QTTY - 1), buffer.buffersEmptyAviable());
  EXPECT_EQ((1), buffer.buffersFullAviable());
}

TEST_F(TestBufferManager, InsertSeveralItemsAndRetrieveThem)
{
  ItemType items[BUFFER_SIZE] = {55, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  buffer.write(items, BUFFER_SIZE);
  DmaBuffer<ItemType> *lBuffer = buffer.buffer();
  assert(lBuffer);
  ItemType *data = lBuffer->data();
  EXPECT_EQ(0, memcmp(items, data, BUFFER_SIZE));
}

TEST_F(TestBufferManager, InsertUnTilFullAndCheckFullAndNotEmpty)
{
  ItemType items[BUFFER_SIZE] = {55, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  while(buffer.write(items, BUFFER_SIZE) != 0)
  {
  }
  EXPECT_TRUE(buffer.buffersEmptyAviable() == 0);
  EXPECT_TRUE(buffer.buffersFullAviable() == BUFFER_QTTY);
}

TEST_F(TestBufferManager, InsertUnTilAllBuffersFullAndRetrieveIt)
{
  size_t   SIZE               = BUFFER_SIZE;
  ItemType items[BUFFER_SIZE] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  while(buffer.buffersEmptyAviable() != 0)
  {
    EXPECT_EQ(SIZE, buffer.write(items, BUFFER_SIZE));
  }
  EXPECT_TRUE(buffer.buffersEmptyAviable() == 0);
  EXPECT_TRUE(buffer.buffersFullAviable() == BUFFER_QTTY);
  DmaBuffer<ItemType> *lBuffer = E_NULLPTR;
  do
  {
    lBuffer = buffer.buffer();
    EXPECT_EQ(0, memcmp(items, lBuffer->data(), BUFFER_SIZE));
  } while(buffer.nextBufferToBeRead());
}
