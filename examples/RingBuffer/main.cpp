#include <stdio.h>

#include <cstdint>

#include "RingBuffer.hpp"
using namespace std;

void FillRingBuffer(eHSM::RingBuffer<uint8_t> *localRingBufferPtr, uint8_t value)
{
  while(!localRingBufferPtr->isFull())
  {
    localRingBufferPtr->write(value);
  }
}

bool CheckRingBuffer(eHSM::RingBuffer<uint8_t> *localRingBufferPtr, uint8_t value)
{
  for(uint32_t index = 0; index < localRingBufferPtr->size(); index++)
  {
    if(localRingBufferPtr->at(index) != value)
    {
      return false;
    }
  }
  return true;
}

int main()
{
  const uint8_t VALUE_TO_FILL1 = 55;
  const uint8_t VALUE_TO_FILL2 = 99;
  // are actually different type of classes, RingBuffer<uint8_t, 16> and
  // RingBuffer<uint8_t, 32>
  eHSM::Declare::RingBuffer<uint8_t, 16> uint8x16RingBuffer;
  eHSM::Declare::RingBuffer<uint8_t, 32> uint8x32RingBuffer;
  // Nevertheless You can use the same pointer (RingBuffer<uint8_t> *) to both
  // classes
  FillRingBuffer(&uint8x16RingBuffer, VALUE_TO_FILL1);
  FillRingBuffer(&uint8x32RingBuffer, VALUE_TO_FILL2);

  if(CheckRingBuffer(&uint8x16RingBuffer, VALUE_TO_FILL1))
  {
    printf("Ok\r\n");
  }
  else
  {
    printf("FAIL\r\n");
  }

  if(CheckRingBuffer(&uint8x32RingBuffer, VALUE_TO_FILL2))
  {
    printf("Ok\r\n");
  }
  else
  {
    printf("FAIL\r\n");
  }
}
