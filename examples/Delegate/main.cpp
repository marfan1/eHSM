#include <stdio.h>

#include <cstdint>

#include "Delegate.hpp"

using namespace std;

class MyClass
{
 public:
  MyClass() {}
  void method()
  {
    printf("MyClass Method called\r\n");
  }
  static void staticMethod()
  {
    printf("MyClass Static method called\r\n");
  }
  void methodWithParameter(int param)
  {
    printf("MyClass Parameter %i called\r\n", param);
  }
};

int main()
{
  MyClass obj;

  eHSM::Delegate<void> delegate;

  delegate.bind<MyClass, &MyClass::method>(&obj);
  delegate();

  delegate.bind<MyClass::staticMethod>();
  delegate();

  eHSM::Delegate<int> delegateInt;
  delegateInt.bind<MyClass, &MyClass::methodWithParameter>(&obj);
  delegateInt(5);
}
