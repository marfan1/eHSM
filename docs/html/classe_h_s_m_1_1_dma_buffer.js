var classe_h_s_m_1_1_dma_buffer =
[
    [ "DmaBuffer", "classe_h_s_m_1_1_dma_buffer.html#aa02a92ffdf94fa977c424c982cd14221", null ],
    [ "at", "classe_h_s_m_1_1_dma_buffer.html#a8c00b3b12fe29829f8cf99a70c66ce30", null ],
    [ "at", "classe_h_s_m_1_1_dma_buffer.html#a00f01c68bce4293f606c9d4d9eb5b2d7", null ],
    [ "data", "classe_h_s_m_1_1_dma_buffer.html#a2d5279d4a71ced77abded133083fe270", null ],
    [ "data", "classe_h_s_m_1_1_dma_buffer.html#a80c72a3b67a0aacbb87bd88325727dd6", null ],
    [ "isEmpty", "classe_h_s_m_1_1_dma_buffer.html#acf4e1fc1250ff620f7ebcd54868f400d", null ],
    [ "isFull", "classe_h_s_m_1_1_dma_buffer.html#a9ecf03f4b3664ceee3d2088ebba5ab45", null ],
    [ "itemsAviable", "classe_h_s_m_1_1_dma_buffer.html#a24f7391e6d4c80e40925678b703684d8", null ],
    [ "reset", "classe_h_s_m_1_1_dma_buffer.html#adc53b2144b6c861734de7839a698d24d", null ],
    [ "setItemsAviable", "classe_h_s_m_1_1_dma_buffer.html#a937d181aeb7fae00f9f3815b507926c7", null ],
    [ "size", "classe_h_s_m_1_1_dma_buffer.html#aa43a3d3d59ede80e53d6cd3e6593a116", null ],
    [ "spaceLeft", "classe_h_s_m_1_1_dma_buffer.html#a5b2674c05435d2250e93fac2eee22938", null ],
    [ "write", "classe_h_s_m_1_1_dma_buffer.html#aca571ff91ea2e3b2e07a0da3b89ff0dd", null ],
    [ "write", "classe_h_s_m_1_1_dma_buffer.html#a0e06996b7fcbc7cb6f827245713228f9", null ],
    [ "bufferPtr_", "classe_h_s_m_1_1_dma_buffer.html#a6e409ce73016452f999ffd902710c7f0", null ],
    [ "bufferSize_", "classe_h_s_m_1_1_dma_buffer.html#a987bfdc140646498afdaa650b41605ea", null ],
    [ "writeCounter_", "classe_h_s_m_1_1_dma_buffer.html#acaf9b5bf615d5af4748d0e8572b5987b", null ]
];