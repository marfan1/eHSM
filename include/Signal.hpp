/*******************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Ernesto Cruz Olivera (ecruzolivera@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

/**
 ******************************************************************************
 * @file    Signal.hpp
 * @author  Ing. Manuel Alejandro Linares Paez
 * @version 1.0
 * @date    Mar 28, 2016
 * @brief   Signal class
 * used to connect slots and notify them when needed.
 * @bug     No known bugs.
 *
 ******************************************************************************
 */

#ifndef SIGNAL_HPP
#define SIGNAL_HPP

#include <cstdint>

#include "Array.hpp"
#include "Delegate.hpp"

namespace eHSM
{
/**
 * @brief The Signal class
 * An Signal object can be connected with multiple slots and notify them
 * whenever its necesary. Slots can only have one parameter and it can be of any
 * type.
 * @tparam SLOTS_MAX Maximum number of slots that can be connected to the
 * signal.
 * @tparam Param Argument type of the slots.
 */
template <typename ParamType, std::uint8_t SLOTS_MAX = 10>
class Signal
{
 public:
  /**
   * @brief Signal constructor
   */
  Signal() {}

  /**
   * @brief Connects a global or static function of any class to this signal.
   * The function can take one parameter and have no return value.
   * @return true if there is space in the slots list.
   * @tparam Method Name of the function to connect.
   *
   * @code
   * void globalFunction(int number){...}
   * .
   * .
   * Signal<int,5> signal;
   * signal.connect<&globalFunction>();
   * @endcode
   */
  template <void (*Method)(ParamType)>
  bool connect()
  {
    if(slotsList.isFull())
    {
      return false;
    }
    Delegate<ParamType> tempDelegate;
    tempDelegate.template bind<Method>();
    slotsList.append(tempDelegate);
    return true;
  }

  /**
   * @brief Connects a public member function of any class to this signal. The
   * function can take one parameter and have no return value.
   * @param obj Address of the object of the class that contains de function.
   * @return true if there is space in the slots list.
   * @tparam T Class that contains the function.
   * @tparam Method Name of the function to connect.
   *
   * @code
   * Signal<int,5> signal;
   * signal.connect<ExampleClass,&ExampleClass::publicFunction>(&obj);
   * @endcode
   */
  template <class T, void (T::*Method)(ParamType)>
  bool connect(T* obj)
  {
    if(slotsList.isFull())
    {
      return false;
    }
    Delegate<ParamType> tempDelegate;
    tempDelegate.template bind<T, Method>(obj);
    slotsList.append(tempDelegate);
    return true;
  }

  /**
   * @brief Connects a public const member function of any class to this signal.
   * The function can take one parameter and have no return value.
   * @param obj Address of the object of the class that contains de function.
   * @return true if there is space in the slots list.
   * @tparam T Class that contains the function.
   * @tparam Method Name of the function to connect.
   *
   * @code
   * Signal<int,5> signal;
   * signal.connect<ExampleClass,&ExampleClass::publicFunction>(&obj);
   * @endcode
   */
  template <class T, void (T::*Method)(ParamType) const>
  bool connect(T const* obj)
  {
    if(slotsList.isFull())
    {
      return false;
    }
    Delegate<ParamType> tempDelegate;
    tempDelegate.template bind<T, Method>(obj);
    slotsList.append(tempDelegate);
    return true;
  }

  /**
   * @brief Disconnects a slot from this signal. This is only done for the first
   * registered item.
   * @tparam Methd Name of the function to disconnect.
   *
   * @code
   * signal.disconnect<&globalFunction>();
   * @endcode
   */
  template <void (*Method)(ParamType)>
  void disconnect()
  {
    Delegate<ParamType> tempDelegate;
    tempDelegate.template bind<Method>();
    int index = slotsList.indexOf(tempDelegate);
    if(index != -1)
    {
      slotsList.remove(index);
    }
  }

  /**
   * @brief Disconnects a slot from this signal. This is only done for the first
   * registered item.
   * @tparam T Class that contains the function.
   * @tparam Methd Name of the function to disconnect.
   *
   * @code
   * signal.disconnect<ExampleClass,&ExampleClass::publicFunction>(&obj);
   * @endcode
   */
  template <class T, void (T::*Method)(ParamType)>
  void disconnect(T* obj)
  {
    Delegate<ParamType> tempDelegate;
    tempDelegate.template bind<T, Method>(obj);
    int index = slotsList.indexOf(tempDelegate);
    if(index != -1)
    {
      slotsList.remove(index);
    }
  }

  /**
   * @brief Disconnects a slot from this signal. This is only done for the first
   * registered item.
   * @tparam T Class that contains the function.
   * @tparam Methd Name of the function to disconnect.
   *
   * @code
   * signal.disconnect<ExampleClass,&ExampleClass::publicFunction>(&obj);
   * @endcode
   */
  template <class T, void (T::*Method)(ParamType) const>
  void disconnect(T const* obj)
  {
    Delegate<ParamType> tempDelegate;
    tempDelegate.template bind<T, Method>(obj);
    int index = slotsList.indexOf(tempDelegate);
    if(index != -1)
    {
      slotsList.remove(index);
    }
  }

  /**
   * @brief Disconnects all slot from this signal.
   */
  void disconnectAll()
  {
    slotsList.clear();
  }

  /**
   * @brief Notifies to all the connected slots.
   * @param val Parameter to pass to the functions connected.
   * @return true if there is any element in the slots list.
   */
  bool notify(ParamType val)
  {
    if(slotsList.isEmpty())
    {
      return false;
    }
    for(std::size_t i = 0; i < slotsList.itemsAviable(); i++)
    {
      slotsList.at(i)(val);
    }
    return true;
  }

  /**
   * @brief Indicates if there are no slots connected to the signal.
   * @retval true if there are no slots connected.
   * @retval false if there is at least one slot connected.
   */
  bool isEmpty()
  {
    return slotsList.isEmpty();
  }

  /**
   * @brief Indicates if all the slots allowed are connected.
   * @retval true if all slots are connected.
   * @retval false if there is room for at least one slot.
   */
  bool isFull()
  {
    return slotsList.isFull();
  }

 protected:
  /**
   * @brief List of slots connected to the signal.
   */
  Declare::Array<Delegate<ParamType>, SLOTS_MAX> slotsList;

 private:
  E_DISABLE_COPY(Signal);

};  // Signal

/**
 * @brief Template specialization of the class
 * template <typename Param = int, std::uint8_t MAX_SLOTS = 10>
 * class Signal{}
 * This is for the special case when the slots take no arguments.
 * @tparam SLOTS_MAX Maximum number of slots that can be connected to the
 * signal.
 */
template <std::uint8_t SLOTS_MAX>
class Signal<void, SLOTS_MAX>
{
 public:
  /**
   * @brief Signal constructor
   */
  Signal<void, SLOTS_MAX>() {}

  /**
   * @brief Connects a global or static function of any class to this signal.
   * The function takes no parameter and have no return value.
   * @return true if there is space in the slots list.
   * @tparam Method Name of the function to connect.
   *
   * @code
   * void globalFunction(void){...}
   * .
   * .
   * Signal<void,5> signal;
   * signal.connect<&globalFunction>();
   * @endcode
   */
  template <void (*Method)(void)>
  bool connect()
  {
    if(slotsList.isFull())
    {
      return false;
    }
    Delegate<void> tempDelegate;
    tempDelegate.bind<Method>();
    slotsList.append(tempDelegate);
    return true;
  }

  /**
   * @brief Connects a public member function of any class to this signal. The
   * function takes no parameter and have no return value.
   * @param obj Address of the object of the class that contains de function.
   * @return true if there is space in the slots list.
   * @tparam T Class that contains the function.
   * @tparam Method Name of the function to connect.
   *
   * @code
   * Signal<void,5> signal;
   * signal.connect<ExampleClass,&ExampleClass::publicFunction>(&obj);
   * @endcode
   */
  template <class T, void (T::*Method)(void)>
  bool connect(T* obj)
  {
    if(slotsList.isFull())
    {
      return false;
    }
    Delegate<void> tempDelegate;
    tempDelegate.bind<T, Method>(obj);
    slotsList.append(tempDelegate);
    return true;
  }

  /**
   * @brief Connects a public const member function of any class to this signal.
   * The function takes no parameter and have no return value.
   * @param obj Address of the object of the class that contains de function.
   * @return true if there is space in the slots list.
   * @tparam T Class that contains the function.
   * @tparam Method Name of the function to connect.
   *
   * @code
   * Signal<void,5> signal;
   * signal.connect<ExampleClass,&ExampleClass::publicFunction>(&obj);
   * @endcode
   */
  template <class T, void (T::*Method)(void) const>
  bool connect(T const* obj)
  {
    if(slotsList.isFull())
    {
      return false;
    }
    Delegate<void> tempDelegate;
    tempDelegate.bind<T, Method>(obj);
    slotsList.append(tempDelegate);
    return true;
  }

  /**
   * @brief Disconnects a slot from this signal. This is only done for the first
   * registered item.
   * @tparam Methd Name of the function to disconnect.
   *
   * @code
   * signal.disconnect<&globalFunction>();
   * @endcode
   */
  template <void (*Method)(void)>
  void disconnect()
  {
    Delegate<void> tempDelegate;
    tempDelegate.bind<Method>();
    int index = slotsList.indexOf(tempDelegate);
    if(index != -1)
    {
      slotsList.remove(index);
    }
  }

  /**
   * @brief Disconnects a slot from this signal. This is only done for the first
   * registered item.
   * @tparam T Class that contains the function.
   * @tparam Methd Name of the function to disconnect.
   *
   * @code
   * signal.disconnect<ExampleClass,&ExampleClass::publicFunction>(&obj);
   * @endcode
   */
  template <class T, void (T::*Method)(void)>
  void disconnect(T* obj)
  {
    Delegate<void> tempDelegate;
    tempDelegate.bind<T, Method>(obj);
    int index = slotsList.indexOf(tempDelegate);
    if(index != -1)
    {
      slotsList.remove(index);
    }
  }

  /**
   * @brief Disconnects a slot from this signal. This is only done for the first
   * registered item.
   * @tparam T Class that contains the function.
   * @tparam Methd Name of the function to disconnect.
   *
   * @code
   * signal.disconnect<ExampleClass,&ExampleClass::publicFunction>(&obj);
   * @endcode
   */
  template <class T, void (T::*Method)(void) const>
  void disconnect(T const* obj)
  {
    Delegate<void> tempDelegate;
    tempDelegate.bind<T, Method>(obj);
    int index = slotsList.indexOf(tempDelegate);
    if(index != -1)
    {
      slotsList.remove(index);
    }
  }

  /**
   * @brief Disconnects all slot from this signal.
   */
  void disconnectAll()
  {
    slotsList.clear();
  }

  /**
   * @brief Notifies to all the connected slots.
   * @param val Parameter to pass to the functions connected.
   * @return true if there is any element in the slots list.
   */
  bool notify(void)
  {
    if(slotsList.isEmpty())
    {
      return false;
    }
    for(std::size_t i = 0; i < slotsList.itemsAviable(); i++)
    {
      slotsList.at(i)();
    }
    return true;
  }

  /**
   * @brief Indicates if there are no slots connected to the signal.
   * @retval true if there are no slots connected.
   * @retval false if there is at least one slot connected.
   */
  bool isEmpty()
  {
    return slotsList.isEmpty();
  }

  /**
   * @brief Indicates if all the slots allowed are connected.
   * @retval true if all slots are connected.
   * @retval false if there is room for at least one slot.
   */
  bool isFull()
  {
    return slotsList.isFull();
  }

 protected:
  /**
   * @brief List of slots connected to the signal.
   */
  Declare::Array<Delegate<void>, SLOTS_MAX> slotsList;

 private:
  E_DISABLE_COPY(Signal);

};  // Signal<void>

}  // namespace eHSM

#endif  // SIGNAL_HPP
